FROM debian:bullseye

RUN apt update -y && apt install -y openjdk-17-jre openjdk-17-jdk wget git
RUN mkdir -p /server/plugins /BuildTools /plugins

WORKDIR /BuildTools
RUN wget https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar
RUN java -jar BuildTools.jar
RUN cp spigot*.jar /server/server.jar

WORKDIR /server

RUN wget https://piston-data.mojang.com/v1/objects/f69c284232d7c7580bd89a5a4931c3581eae1378/server.jar
RUN java -jar server.jar nogui
RUN sed -i s/false/true/ eula.txt 

COPY ./entrypoint.sh /entrypoint.sh

EXPOSE 25565

CMD /entrypoint.sh
