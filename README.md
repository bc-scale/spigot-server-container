# eyegog's Minecraft spigot server container
A minecraft spigot server in a docker container.

## Usage

Pull the image:
```
docker pull eyegog/spigot_server
```

Run the container with no mounts:
```
docker run --name "normal" eyegog/spigot_server:latest
```

Run the container with mounted world directories and configuration, and expose port `25566`:
```
docker run --name "normal" \
	-v "/srv/spigot/servers/normal/plugins:/plugins" \
	-v "/srv/spigot/servers/normal/server.properties:/server/server.properties" \
	-v "/srv/spigot/servers/normal/world:/server/world" \
	-v "/srv/spigot/servers/normal/world_the_nether:/server/world_the_nether" \
	-v "/srv/spigot/servers/normal/world_the_end:/server/world_the_end" \
	-p "25566:25565" \
	eyegog/spigot_server:latest
```

### Configuration

The server binaries and config are located at `/server` within the container. After the container has been run and the binary generates the inital configuration, `/server` will look like this:
```
/server
|-- banned-ips.json
|-- banned-players.json
|-- bukkit.yml
|-- bundler
|-- commands.yml
|-- eula.txt
|-- help.yml
|-- logs
|-- ops.json
|-- permissions.yml
|-- plugins
|-- server-icon.png
|-- server.jar
|-- server.properties
|-- spigot.yml
|-- usercache.json
|-- whitelist.json
|-- world
|-- world_nether
`-- world_the_end
```

**Using predefined confgiuration**
- You can [bind mount](https://docs.docker.com/storage/bind-mounts/) your own configuration and world files into this directory for use at runtime.

**Plugins**
- The server will generate several files in `/server/plugins`. These files shouldn't be destroyed, thus if you want to bind mount a directory to `/server/plugins` it is recommended to instead mount plugin binaries to `/plugins`. All files found in here will be copied into `/server/plugins` at runtime.
